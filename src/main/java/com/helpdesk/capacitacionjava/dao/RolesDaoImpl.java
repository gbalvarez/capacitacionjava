/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.dao;

import com.helpdesk.capacitacionjava.modelos.Roles;
import com.helpdesk.capacitacionjava.util.NewHibernateUtil;
import java.util.List;
import javax.ejb.Stateless;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author David Ramirez
 */
@Stateless
public class RolesDaoImpl implements RolesDao {

    @Override
    public List<Roles> listaRoles() {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        List<Roles> listaRoles = session.createQuery("From Roles").list();
        transaction.commit();
        return listaRoles;
    }

    @Override
    public Roles obtenerRol(Integer idRol) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        transaction = session.beginTransaction();
        return (Roles) session.get(Roles.class, idRol);
    }

}
